//
//  TopTab.swift
//  YMTopTab_Swift
//
//  Created by admin on 14/11/25.
//  Copyright (c) 2014年 barryclass. All rights reserved.
//

import UIKit
var screen_width = UIScreen.mainScreen().bounds.width
var screen_height = UIScreen.mainScreen().bounds.height
var width = Int(UInt32(screen_width))
var height = Int(UInt32(screen_height))



class TopTab: UIView, UIScrollViewDelegate {
    
    
    var selected : Int = 0
    var mask1View: UIView?
    
    var scrollView: UIScrollView?
    var adapterScrollView: UIScrollView!
    var buttonArray: Array<UIButton> = Array<UIButton>()
    
    
    
    //可以自定义的属性
    var item_width = 75
    var item_bg_color = UIColor.groupTableViewBackgroundColor()
    var item_font_color = UIColor.darkGrayColor()
    var slider_color = UIColor.blackColor()

    init(tabsArray: [String],controllers: [UIViewController]) {
        super.init()
        self.frame = CGRectMake(0, 64, screen_width, 600)
        if item_width * tabsArray.count < width{
            item_width = width / tabsArray.count
        }
        self.backgroundColor = UIColor.whiteColor()
        
        setTopItem(tabsArray)
        setSliderView()
        setContent(controllers)
    }
    
    
    
    func setTopItem(tabsArray: [String]){
        var frameScroll = CGRect(x: 0, y: 0, width: screen_width, height: 40)
        scrollView = UIScrollView(frame: frameScroll)
        scrollView?.contentSize = CGSize(width: item_width * tabsArray.count, height: 0)
        scrollView?.backgroundColor = item_bg_color
        scrollView?.showsHorizontalScrollIndicator = false
        scrollView?.tag = 0
        
        for var i=0; i < tabsArray.count; i++ {
            var frame = CGRect(x: i*item_width, y: 0, width: item_width, height: 40)
            var button = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
            button.setTitle(tabsArray[i], forState: UIControlState.Normal)
            button.frame = frame
           // button.tintColor = UIColor.blackColor()
            button.tag = i
            
            button.setTitleColor(item_font_color, forState: UIControlState.Normal)
            button.addTarget(self, action: "itemChanged:", forControlEvents: UIControlEvents.TouchUpInside)
            
            buttonArray.append(button)
            self.scrollView?.addSubview(button)
            
        }

    }
    
    
    func setSliderView(){
        var maskFrame = CGRect(x: selected*item_width, y: 38, width: item_width, height: 2)
        self.mask1View = UIView(frame: maskFrame)
        self.mask1View!.backgroundColor = slider_color
        
        self.scrollView?.addSubview(mask1View!)
        self.scrollView?.delegate = self
        self.scrollView?.autoresizesSubviews = true
        self.addSubview(scrollView!)
    }
    
    func setContent(controllers: [UIViewController]){
        var colorArray: NSArray = [UIColor.orangeColor(), UIColor.brownColor(), UIColor.grayColor(), UIColor.redColor(), UIColor.blueColor(), UIColor.purpleColor()]
        
        
        adapterScrollView = UIScrollView(frame: CGRectMake(0, 40, screen_width, self.bounds.height - 40))
        self.addSubview(adapterScrollView)
        
        
        adapterScrollView.tag = 1
        adapterScrollView.contentSize = CGSizeMake(4 * screen_width, 10);
        adapterScrollView.pagingEnabled = true
        adapterScrollView.delegate = self;
        adapterScrollView.showsHorizontalScrollIndicator = false
        
        for var i=0; i < controllers.count; i++ {
            var view = controllers[i].view
            view.frame = CGRect(x: i*width, y: 0, width: width, height: height - 40)
            adapterScrollView.addSubview(view)
        }
    }
    

    
    
    func itemChanged(sender: AnyObject){
        var selected = (sender as UIButton).tag
        var maskFrame = CGRect(x: selected*item_width, y: 38, width: item_width, height: 2)
        self.mask1View?.frame = maskFrame
        self.adapterScrollView?.setContentOffset(CGPoint(x: selected*width, y: 0), animated: false)
    }
    
    

    
    
    func itemChanged_scroll(scrollView: UIScrollView){
        var kSelected: CGFloat =  ceil(scrollView.contentOffset.x / scrollView.frame.size.width)
        var temp = UInt32(kSelected)
        var temp_1: Int = Int(temp)
        var maskFrame = CGRect(x: temp_1*item_width, y: 38, width: item_width, height: 2)
        self.mask1View?.frame = maskFrame
    }
    
    
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView.tag == 0{
            self.itemChanged(scrollView)
        }else if scrollView.tag == 1{
            itemChanged_scroll(scrollView)
        }else {
            println("----error----")
        }
        
    
    }// any offset changes
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
