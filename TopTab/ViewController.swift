//
//  ViewController.swift
//  TopTab
//
//  Created by admin on 14/11/27.
//  Copyright (c) 2014年 sinosun. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var topTab: TopTab?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var view = UIView(frame: CGRectMake(0, 0, 320, 0))
        self.view.addSubview(view)
        var controllers: [UIViewController] = [FirstViewController(), SecondViewController(), ThirdViewController(), FourViewController()]
        var tabsArray:[String] = ["1", "2", "3", "4"]
        self.topTab = TopTab(tabsArray: tabsArray, controllers: controllers)
        self.view.addSubview(self.topTab!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

