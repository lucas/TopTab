#TopTab
#功能描述
按标签分类展示不同内容的视图，通过左右滑动或点击屏幕上方的标签切换视图
#demo
var view = UIView(frame: CGRectMake(0, 0, 320, 0))
self.view.addSubview(view)
var controllers: [UIViewController] = [FirstViewController(), SecondViewController(), ThirdViewController(), FourViewController()]
var tabsArray:[String] = ["1", "2", "3", "4"]
self.topTab = TopTab(tabsArray: tabsArray, controllers: controllers)
self.view.addSubview(self.topTab!)

#demo截图
![image](http://git.oschina.net/lucas/TopTab/raw/master/jp/2.png)
#代办事项
动态增加，删除标签
